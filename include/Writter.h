#include <string>
#include "../include/DelFile.h"

class Writter
{
	private:
		std::string filename = "Thinker.log";
		int fileSize;
		DelFile delfile;
	public:
		Writter();
		void setNumberOfFilesAllowed(int filesAllowed);
		void setFileSize(int size);
		void rotate();
		void write(std::string message);
		int getFileSize(std::string filename);
};
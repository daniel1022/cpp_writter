#include <string>
#include <queue>

class DelFile
{
	private:
		std::queue<std::string> dropFiles;
		int filesAllowed;
	public:
		DelFile();
		void set(int fAllowed);
		void toQueue(const char* fileName);
		void deleteFile(std::string fileName);	
		void readFileList();
		void saveFileList();
};
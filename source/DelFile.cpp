#include "../include/DelFile.h"
#include <stdio.h>
#include <string>
#include <fstream>

using namespace std;
	
	DelFile::DelFile(){}

	void DelFile::set(int fAllowed){
		filesAllowed = fAllowed;
	}

	void DelFile::toQueue(const char* fileName){
		readFileList();
		std::string st(fileName);  // using a const into a queue makes it work like a stack
		dropFiles.push (st);
		int tam = dropFiles.size();
		printf("********** %d %s\n", tam, st.c_str());
		if(dropFiles.size() > filesAllowed){
			printf("front %s\n", dropFiles.front().c_str() );
			deleteFile(dropFiles.front());
			dropFiles.pop();
		}
		saveFileList(); 
	}

	void DelFile::deleteFile(string fileName)
	{
	  if(remove(fileName.c_str()) != 0)
	  	printf("%s %s\n", "Error deleting file: ", fileName.c_str());
	  else
	    printf("%s %s %s\n", "File ", fileName.c_str(), " successfully deleted" );
	} 

	void DelFile::saveFileList(){
		ofstream log;
  		log.open("filesLeft.txt");
  		while (!dropFiles.empty()){
			log << dropFiles.front() << endl; 
			dropFiles.pop();		
  		}
  		log.close();
	}	

	void DelFile::readFileList(){
		std::ifstream infile("filesLeft.txt");
		std::string line;
		while (infile >> line)
		{
		    dropFiles.push(line);
		}
	}	
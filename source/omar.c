#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

/* Función para devolver un error en caso de que ocurra */
void error(const char *s);

/* Calculamos el tamaño del archivo */
long fileSize(char *fname);

/* Sacamos el tipo de archivo haciendo un stat(), es como el stat de la línea de comandos */
unsigned char statFileType(char *fname);

/* Intenta sacar el tipo de archivo del ent */
unsigned char getFileType(char *ruta, struct dirent *ent);

/* Obtiene el nombre del fichero con la ruta completa */
char *getFullName(char *ruta, struct dirent *ent);

//Obtiene 1 si es archivo de tipo texto 
int getTypeText(const char* x);

// Separa la extencion del nombre del archivo
char* substr(char* cadena, int comienzo, int longitud);


/* Función principal, que cuenta archivos */
unsigned cuentaArchivos(char *ruta);
void eliminarArchivos(const char* ruta,const char* name);
char* lastName(char* ruta);


int main(int argc, char *argv[])
{
  unsigned num;
  if (argc != 2)
    {
      error("Uso: ./Directorio\n");
    }
  printf("Directorio 1: %s\n", argv[1]);
  num=cuentaArchivos("./");
  //char* ba=lastName(argv[1]);//muestra el menor de los valores
  eliminarArchivos("prueva",lastName("prueva"));
  printf("%s . Total: %u archivos\n", argv[1], num);
  /* Empezaremos a leer en el directorio actual */
  return EXIT_SUCCESS;
}


void error(const char *s)
{
  /* perror() devuelve la cadena S y el error (en cadena de caracteres) que tenga errno */
  perror (s);
  exit(EXIT_FAILURE);
}

char *getFullName(char *ruta, struct dirent *ent)
{
  char *nombrecompleto;
  int tmp;
  tmp=strlen(ruta);
  nombrecompleto=malloc(tmp+strlen(ent->d_name)+2); /* Sumamos 2, por el \0 y la barra de directorios (/) no sabemos si falta */
  sprintf(nombrecompleto,"%s", ent->d_name);
  return nombrecompleto;
}


unsigned cuentaArchivos(char *ruta)
{
  /* Con un puntero a DIR abriremos el directorio */
  DIR *dir;
  /* en *ent habrá información sobre el archivo que se está "sacando" a cada momento */
  struct dirent *ent;
  unsigned numfiles=0;          /* Ficheros en el directorio actual */
  unsigned char tipo;       /* Tipo: fichero /directorio/enlace/etc */
  char *nombrecompleto;     /* Nombre completo del fichero */
  dir = opendir (ruta);
  /* Miramos que no haya error */
  if (dir == NULL)
    error("No puedo abrir el directorio");
 
  while ((ent = readdir (dir)) != NULL)
    {
      if ( (strcmp(ent->d_name, ".")!=0) && (strcmp(ent->d_name, "..")!=0) )
      {
        nombrecompleto=getFullName(ruta, ent);
        char* var[100];
        *var = nombrecompleto;
        tipo=getFileType(nombrecompleto, ent);
        if (tipo==DT_REG)
          {
            int g = getTypeText(*var);
            if (g==1) { 
              ++numfiles;
              printf("%s\n",nombrecompleto);
            }
          }
       free(nombrecompleto);
      }
    }
  closedir (dir);
 
  return numfiles;
}

char* lastName(char *ruta)
{
  /* Con un puntero a DIR abriremos el directorio */
  DIR *dir;
  /* en *ent habrá información sobre el archivo que se está "sacando" a cada momento */
  struct dirent *ent;
  unsigned char tipo;       /* Tipo: fichero /directorio/enlace/etc */
  char *nombrecompleto;     /* Nombre completo del fichero */
  dir = opendir (ruta);
  int numeroTexto=0;
  char* comparador;
  /* Miramos que no haya error */
  if (dir == NULL)
    error("No puedo abrir el directorio");
 
  while ((ent = readdir (dir)) != NULL)
    {
      if ( (strcmp(ent->d_name, ".")!=0) && (strcmp(ent->d_name, "..")!=0) )
      {
        nombrecompleto=getFullName(ruta, ent);
        tipo=getFileType(nombrecompleto, ent);
        if (tipo==DT_REG)
          {
            int h = strlen(nombrecompleto);
            if (numeroTexto==0)
            {
              comparador = substr(nombrecompleto,0,h-4);
              numeroTexto = atoi(comparador); 
            }
            else{
              comparador = substr(nombrecompleto,0,h-4);
              int numeroTexto2 = atoi(comparador); 
              if (numeroTexto2 < numeroTexto)
              {
                numeroTexto=numeroTexto2;
              }
            }
          }
       free(nombrecompleto);
      }
    }
    char mandar[40];
    sprintf(mandar,"%d",numeroTexto);
    strcat(mandar,".txt");
    comparador=mandar;
  closedir (dir);
  return comparador;
}

















void eliminarArchivos(const char* ruta,const char* name)
{
  char str[100];
  strcpy(str,ruta);
  strcat(str,"/");
  strcat(str,name);// 
  printf("%s\n", str);
  remove(str);
} 

int getTypeText(const char* x)
{
  int texto=0;
  char* var = (char *)x;
  int h = strlen(x);
  char* comp=".txt";
  if (h>4){
    var = substr(var,h-4,4);
    if (!strcmp(var,comp)){ 
      texto = 1;
    }
    else{
    texto = 0;
    }
  }
  return texto;
}

char* substr(char* cadena, int comienzo, int longitud)
{
    if (longitud == 0) 
        longitud = strlen(cadena)-comienzo;
    
    char *nuevo = (char*)malloc(sizeof(char) * (longitud+1));
    nuevo[longitud] = '\0';
    strncpy(nuevo, cadena + comienzo, longitud);
    
    return nuevo;
}


unsigned char getFileType(char *nombre, struct dirent *ent)
{
  unsigned char tipo;
  tipo=ent->d_type;
  if (tipo==DT_UNKNOWN)
    {
      tipo=statFileType(nombre);
    }
  return tipo;
}

/* stat() vale para mucho más, pero sólo queremos el tipo ahora */
unsigned char statFileType(char *fname)
{
  struct stat sdata;
  /* Intentamos el stat() si no funciona, devolvemos tipo desconocido */
  if (stat(fname, &sdata)==-1)
    {
      return DT_UNKNOWN;
    }
  switch (sdata.st_mode & S_IFMT)
    {
    case S_IFBLK:  return DT_BLK;
    case S_IFCHR:  return DT_CHR;
    case S_IFDIR:  return DT_DIR;
    case S_IFIFO:  return DT_FIFO;
    case S_IFLNK:  return DT_LNK;
    case S_IFREG:  return DT_REG;
    case S_IFSOCK: return DT_SOCK;
    default:       return DT_UNKNOWN;
    }
}

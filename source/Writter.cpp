// basic file operations g++ -o ./output/log Writter.cpp -std=c++11
#include <iostream>
#include <fstream>
#include <ctime>
#include <unistd.h>
#include "../include/Writter.h"

using namespace std;

	Writter::Writter(){}

	void Writter::setFileSize(int size){
		fileSize = size;
	}

	void Writter::setNumberOfFilesAllowed(int filesAllowed){
		delfile.set(filesAllowed);
	}

	void Writter::rotate(){
		time_t now = time(0);
		const char* newName = (to_string(now) + ".log").c_str();
  		rename("Thinker.log", newName);
  		delfile.toQueue(newName);
	}

	void Writter::write(string message){

		ofstream log;
  		log.open(filename.c_str(), ios_base::app);
  		log << message << endl;
  		log.close();
  		int size = getFileSize(filename);
  		if(size > fileSize)
  			rotate();
	}

	int Writter::getFileSize(string filename) // path to file
	{
	    FILE *p_file;
	    p_file = fopen(filename.c_str(),"rb");
	    fseek(p_file,0,SEEK_END);
	    int size = ftell(p_file);
	    fclose(p_file);
	    return size;
	}
